"""opendonation URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from donate import views as donate_views
from homepage import views as homepage_views


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^donate/$', donate_views.my_donation),
    url(r'^donate/(?P<id>\d+)$', donate_views.donation_detail),
    url(r'^donate/send/', donate_views.form_give_donation),
    url(r'^donate/give/', donate_views.give_donation),
    url(r'^login/', homepage_views.login_view),
    url(r'^logout/', homepage_views.logout_view)
    
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
