from django.db import models
from django.contrib.auth.models import User


class UserTransaction(models.Model):
    donatur = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True, related_name='donatur')
    amount = models.IntegerField()
    bank_name = models.CharField(max_length=30)
    bank_account_no = models.CharField(max_length=30)
    bank_account_holder = models.CharField(max_length=30)
    payment_proof = models.ImageField(upload_to="payment/")
    status = models.CharField(max_length=10)
    created_at = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return "%s - %s - %s" %(self.id, self.amount, self.bank_name)


class Donation(models.Model):
    transaction_reference = models.ForeignKey(UserTransaction, on_delete=models.SET_NULL, blank=True, null=True)
    confirmed_by = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True, related_name='confirmed_by')
    created_at = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return "%s - %s" % (self.id, self.transaction_reference.donatur.username)
