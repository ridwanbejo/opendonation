from django.contrib import admin
from .models import UserTransaction, Donation
from django.utils.html import mark_safe


# Register your models here.
class UserTransactionAdmin(admin.ModelAdmin):
    list_display = ['donatur', 'amount', 'bank_identity', 'payment_proof', \
                    'status', 'created_at']
    list_filter = ('bank_name', 'status')
    search_fields = ['bank_account_no', 'bank_account_holder']
    list_per_page = 25
    date_hierarchy = 'created_at'

    readonly_fields = ["payment_proof"]
    actions = ['confirm_payment']

    def bank_identity(self, obj):
        return """Account Holder: %s\nBank Name: %s\nAccount No: %s""" % \
            (obj.bank_account_holder, obj.bank_name, obj.bank_account_no)
    
    def payment_proof(self, obj):
        return mark_safe('<img src="{url}" width="{width}" height={height} />' \
            .format(
                url=obj.payment_proof.url,
                width=obj.payment_proof.width,
                height=obj.payment_proof.height,
            )
        )

    def confirm_payment(self, request, queryset):
        for item in queryset:
            if item.status != 'success':
                donation = Donation(
                    transaction_reference=item,
                    confirmed_by=request.user
                )

                donation.save()

                item.status = 'success'
                item.save()

    confirm_payment.short_description = "Confirm payment proof"

admin.site.register(UserTransaction, UserTransactionAdmin)


class DonationAdmin(admin.ModelAdmin):
    list_display = ['transaction_reference',  \
                    'bank_identity', 'created_at']
    list_filter = ('confirmed_by',)
    list_per_page = 25
    date_hierarchy = 'created_at'

    def bank_identity(self, obj):
        return """Account Holder: %s | Bank Name: %s | Account No: %s""" % \
            (obj.transaction_reference.bank_account_holder, \
            obj.transaction_reference.bank_name, \
            obj.transaction_reference.bank_account_no)


admin.site.register(Donation, DonationAdmin)
