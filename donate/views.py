from django.shortcuts import render, redirect
from .models import UserTransaction, Donation
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.conf import settings
import os


# Create your views here.
@login_required(login_url=settings.LOGIN_URL)
def my_donation(request):
    user_donations = UserTransaction.objects.filter(donatur=request.user).order_by('-created_at')
    return render(request, 'new/index.html', {"user": request.user, "user_donations":user_donations})


@login_required(login_url=settings.LOGIN_URL)
def form_give_donation(request):
    return render(request, 'new/donate.html')


@login_required(login_url=settings.LOGIN_URL)
def give_donation(request):
    user_transaction = UserTransaction(
        amount = request.POST['form-1-amount'],
        bank_name = request.POST['form-1-bank_name'],
        bank_account_no = request.POST['form-1-bank_account_no'],
        bank_account_holder = request.POST['form-1-bank_account_holder'],
        status='pending',
        donatur= User.objects.get(pk=2)
    )

    user_transaction.payment_proof = request.FILES['form-1-payment-proof']
    user_transaction.save()
    
    return redirect('/donate')


@login_required(login_url=settings.LOGIN_URL)
def donation_detail(request, id):
    user_donation = UserTransaction.objects.get(pk=id)
    if user_donation.status == "success":
        donation = Donation.objects.get(transaction_reference=user_donation)
        payload = {"item":user_donation, "donation":donation}
    else:
        payload = {"item":user_donation}

    return render(request, 'new/detail.html', payload)
