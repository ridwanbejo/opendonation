from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, logout, login
from django.contrib import messages


def login_view(request):
    if request.POST:
        user = authenticate(
            username=request.POST['form-1-username'],
            password=request.POST['form-1-password']
        )

        if user is not None:
            if user.is_active:
                login(request, user)
                
                request.session['username'] = request.POST['form-1-username']
    
                return redirect('/donate')
    return render(request, 'new/login.html')


def logout_view(request):
    logout(request)
    return redirect('/login/')
