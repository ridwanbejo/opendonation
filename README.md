Open Donation
=============

Sebuah aplikasi *web* yang dirancang untuk melayani donasi secara *online* dengan berbasis pada skema *consumer-to-business* (C2B).

Aplikasi *web* ini dikembangkan dengan menggunakan:

* Python 3.6
* Django 2.0
* SQLite3
* Visual Studio Code
* Ubuntu Linux

Tim Penyusun:

* Andri Hadiansyah
* Iip Saepurahman
* Marshella Nikita Lantang
* Ridwan Fadjar Septian
* Siswanto

E-Business, Magister Sistem Informasi, Pascasarjana, Universitas Komputer Indonesia, 2019.