FROM python:3.6

RUN apt-get update && apt-get install -y netcat gettext

ENV PYTHONUNBUFFERED 1

RUN mkdir /opendonation

WORKDIR /opendonation

ADD requirements.txt /opendonation/

RUN pip install -r requirements.txt

RUN pip install gunicorn gevent eventlet

ADD . /opendonation/

RUN python3.6 manage.py runserver
